#!/usr/bin/python

# Author: Devon Merner (dmerner)
# Date: April 20th, 2021
# Purpose: To track the thread count on a system

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Default exit code
EXIT_CODE = STATE_UNKNOWN

import sys
import argparse
import subprocess
import json

parser = argparse.ArgumentParser()
parser.add_argument("-d", action="store_true", dest="debug", help="Print debug information")
parser.add_argument("-w", default=100000, dest="warningthreshold", help="The warning threshold for total threads.")
parser.add_argument("-c", default=200000, dest="criticalthreshold", help="The critical threshold for total threads.")

options = parser.parse_args()

output_text = ""
perfdata = ""

def main():
    global output_text
    global perfdata
    global EXIT_CODE

    out = subprocess.Popen(['cat', '/proc/loadavg'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout = out.stdout.read()

    if options.debug:
        print(stdout)

    values = stdout.split()

    threads = values[3].split('/')
    runnable_threads = threads[0]
    total_threads = threads[1]

    perfdata += " total_threads=" + total_threads + ";" + str(options.warningthreshold) + ";" + str(options.criticalthreshold)
    perfdata += " runnable_threads=" + runnable_threads

    if (options.criticalthreshold > 0 and float(total_threads) > float(options.criticalthreshold)):
        output_text = "CRITICAL: " + str(runnable_threads) + " runnable thread(s) out of "  + str(total_threads) + " total."
        EXIT_CODE = STATE_CRITICAL

    elif (options.warningthreshold > 0 and float(total_threads) > float(options.warningthreshold)):
        output_text = "WARNING: " + str(runnable_threads) + " runnable thread(s) out of "  + str(total_threads) + " total."
        EXIT_CODE = STATE_WARNING
    else:
        output_text = "OK: " + str(runnable_threads) + " runnable thread(s) out of "  + str(total_threads) + " total."



    print(output_text + " |" + perfdata)

    # Critical/Warning Think Logic
    if EXIT_CODE == STATE_CRITICAL:
        sys.exit(STATE_CRITICAL)
    elif EXIT_CODE == STATE_WARNING:
        sys.exit(STATE_WARNING)
    else:
        sys.exit(STATE_OK)


if __name__ == "__main__":
    main()
