#!/bin/sh

# Author: Anthony Brennan (a2brenna) and Fraser Gunn (fhgunn)
# Date: July 2016, Dec. 2016
# Purpose: To monitor upper bound latest-ish mysql write

# Usage: ./check_mysql_cluster_timestamp $HOST

HOST=$1

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

Mysql=/usr/bin/mysql
Awk=/usr/bin/awk
Tr=/usr/bin/tr

OUTPUT=`$Mysql --defaults-file=/etc/mysql/nagios_check.cnf -h "$HOST" -e "select now() as server_now, client_hostname, server_time as last_update, timestampdiff(second, server_time, now()) as age from nagios_check.last_update where client_hostname = 'ubuntu1404-202'\G" 2>&1`

if [ $? -ne 0 ]
then
        echo "CRITICAL: mysql command failed: $OUTPUT"
        exit $STATE_CRITICAL
fi

AGE=`echo "$OUTPUT" | $Awk '/^ *age: *[0-9]+$/ {print $2}'`

if [ -z "$AGE" ]
then
        echo UNKNOWN: `echo "$OUTPUT" | $Tr '\012' ' '`
        exit $STATE_UNKNOWN
elif [ "$AGE" -lt "302" ]
then
        echo "OK: Timestamp age is $AGE seconds | $OUTPUT"
        exit $STATE_OK
elif [ "$AGE" -lt "600" ]
then
        echo "WARNING: Timestamp age is $AGE seconds | $OUTPUT"
        exit $STATE_WARNING
else
        echo "CRITICAL: Timestamp age is $AGE seconds | $OUTPUT"
        exit $STATE_CRITICAL
fi
