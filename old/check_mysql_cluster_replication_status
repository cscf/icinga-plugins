#!/bin/sh

Host="$1"

State_ok=0
State_warning=1
State_critical=2
State_unknown=3

Mysql=/usr/bin/mysql
Awk=/usr/bin/awk
Sort=/usr/bin/sort
Sed=/bin/sed
Tr=/usr/bin/tr
Grep=/bin/grep


if SlaveStatus=`$Mysql --defaults-file=/etc/mysql/nagios_check.cnf --host "$Host" -e 'show slave status\G' 2>&1`
then
        :
else
        echo "CRITICAL: 'show slave status' failed | $SlaveStatus"
        exit $State_critical
fi

Output=`if [ -z "$SlaveStatus" ]
then
        echo 'OK: not a slave'
else

        echo "$SlaveStatus" | $Awk '
                /^\*+ 1\. row \*+$/  {next}
                /^                *Slave_IO_State: Waiting for master to send event$/  {next}
                /^                   *Master_Host: mysql[a-z0-9.-]+\.uwaterloo\.ca$/  {next}
                /^                   *Master_User: mysql[[a-z0-9._-]*slave$/  {next}
                /^                   *Master_Port: 3306$/  {next}
                /^                 *Connect_Retry: 60$/  {next}
                /^               *Master_Log_File: binlog\.[0-9]+$/  {next}
                /^           *Read_Master_Log_Pos: [0-9]+$/  {next}
                /^                *Relay_Log_File: mysqld-relay-bin\.[0-9]+$/  {next}
                /^                 *Relay_Log_Pos: [0-9]+$/  {next}
                /^         *Relay_Master_Log_File: binlog\.[0-9]+$/  {next}
                /^               *Replicate_Do_DB: $/  {next}
                /^           *Replicate_Ignore_DB: $/  {next}
                /^            *Replicate_Do_Table: $/  {next}
                /^        *Replicate_Ignore_Table: $/  {next}
                /^       *Replicate_Wild_Do_Table: $/  {next}
                /^   *Replicate_Wild_Ignore_Table: $/  {next}
                /^                  *Skip_Counter: 0$/  {next}
                /^           *Exec_Master_Log_Pos: [0-9]+$/  {next}
                /^               *Relay_Log_Space: [0-9]+$/  {next}
                /^               *Until_Condition: None$/  {next}
                /^                *Until_Log_File: $/  {next}
                /^                 *Until_Log_Pos: 0$/  {next}
                /^            *Master_SSL_Allowed: No$/  {next}
                /^            *Master_SSL_CA_File: $/  {next}
                /^            *Master_SSL_CA_Path: $/  {next}
                /^               *Master_SSL_Cert: $/  {next}
                /^             *Master_SSL_Cipher: $/  {next}
                /^                *Master_SSL_Key: $/  {next}
                /^ *Master_SSL_Verify_Server_Cert: No$/  {next}
                /^   *Replicate_Ignore_Server_Ids: $/  {next}
                /^              *Master_Server_Id: [1-9][0-9]*$/  {next}

                /^              *Slave_IO_Running: /  {if ($2 != "Yes")  print "CRITICAL:", $0; next}
                /^             *Slave_SQL_Running: /  {if ($2 != "Yes")  print "CRITICAL:", $0; next}
                /^                    *Last_Errno: /  {if ($2 != "0")    print "CRITICAL:", $0; next}
                /^                    *Last_Error: /  {if ($2 != "")     print "CRITICAL:", $0; next}
                /^                 *Last_IO_Errno: /  {if ($2 != "0")    print "CRITICAL:", $0; next}
                /^                 *Last_IO_Error: /  {if ($2 != "")     print "CRITICAL:", $0; next}
                /^                *Last_SQL_Errno: /  {if ($2 != "0")    print "CRITICAL:", $0; next}
                /^                *Last_SQL_Error: /  {if ($2 != "")     print "CRITICAL:", $0; next}

                /^         *Seconds_Behind_Master: [0-9]+$/  {
                                                                if ($2 >= 20)
                                                                        print "CRITICAL: Slave is", $2, "seconds behind master"
                                                                else if ($2 >= 10)
                                                                        print "WARNING: Slave is", $2, "seconds behind master"
                                                                else
                                                                        print "OK: Slave is", $2, "seconds behind master"
                                                                next
                                                        }

                {print "UNKNOWN:", $0}
        '

fi`

echo "$Output" | $Sort | $Sed -e 's/   */ /' -e 's/:\([^:]*\): */:\1=/' -e 's/$/ \//' | $Tr '\012' ' ' | $Sed 's, / *$,,' ; echo

if echo "$Output" | $Grep -q CRITICAL ; then exit $State_critical ; fi
if echo "$Output" | $Grep -q WARNING  ; then exit $State_warning  ; fi
if echo "$Output" | $Grep -q -v '^OK' ; then exit $State_unknown  ; fi

exit $State_ok
