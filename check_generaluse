#!/usr/bin/python

# Author: Devon Merner (dmerner)
# Date: July 8th, 2020
# Purpose: To monitor the login, home directory status and filesystem stats of a CS general use linux server.

STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Default exit code
EXIT_CODE = STATE_OK

import sys
import argparse
import paramiko
import socket
from datetime import datetime

parser = argparse.ArgumentParser()
parser.add_argument("-t", default=5, dest="timeout",
                    help="SSH command timeout in seconds")
parser.add_argument("-u", default="", dest="username", help="SSH Username")
parser.add_argument("-p", default="", dest="password", help="SSH Password")
parser.add_argument("host", default="localhost", nargs="?",
                    help="Agent to retrieve variables from")
parser.add_argument("-m", default="/srv/DFSc", dest="mountpoint",
                    help="Default mount point to use with Ceph stats (default: /srv/DFSc)")
parser.add_argument("-d", action="store_true", dest="debug",
                    help="Print debug information")

options = parser.parse_args()

ssh = paramiko.SSHClient()

ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

try:
    ssh.connect(options.host, username=options.username, password=options.password, timeout=10)
except socket.gaierror:
    print("CRITICAL - (" + options.host + ") Name or service not known. |")
    sys.exit(STATE_CRITICAL)
except paramiko.ssh_exception.NoValidConnectionsError:
    print("CRITICAL - Unable to connect to port 22 on " + options.host + " |")
    sys.exit(STATE_CRITICAL)
except (paramiko.ssh_exception.SSHException, paramiko.ssh_exception.AuthenticationException) as e:
    print("CRITICAL - Authentication to host " + options.host + " failed. |")
    sys.exit(STATE_CRITICAL)

PERFDATA = ""

def getStats(mount):
    global PERFDATA

    stdin, stdout, stderr = ssh.exec_command('getfattr -d -n ceph.dir.rbytes ' + mount + ' --only-values --absolute-names ; echo')

    for line in stderr:
        if mount in line.rstrip() and "Permission denied" in line.rstrip():
            return

    for line in stdout:
        if line.rstrip() == "0":
            return

        PERFDATA += "ceph.dir.rbytes_" + mount + "=" + line.rstrip() + " "
        total_size = line.rstrip()

    stdin, stdout, stderr = ssh.exec_command('getfattr -d -n ceph.dir.rfiles ' + mount + ' --only-values --absolute-names ; echo')

    for line in stdout:
        if line.rstrip() == "0":
            return
        PERFDATA += "ceph.dir.rfiles_" + mount + "=" + line.rstrip() + " "
        total_count = line.rstrip()

    if (len(total_size) != 0):
        PERFDATA += "ceph.dir.avgfsize_" + mount + "=" + str(int(total_size) / int(total_count)) + " "

def getCommandSingleOutput(command):
    stdin, stdout, stderr = ssh.exec_command(command, timeout=int(options.timeout))

    for line in stdout:
        return line.rstrip()

    return ""

def getCommandMultiOutput(command):
    lines = []

    stdin, stdout, stderr = ssh.exec_command(command, timeout=int(options.timeout))

    for line in stdout:
        lines.append(line.rstrip())

    return lines

def getConnectedPeerHostname():
    return socket.getnameinfo((ssh.get_transport().getpeername()[0], 22), socket.NI_NUMERICSERV)[0]

# Grab the ceph stats
try:
    fstdout = getCommandMultiOutput('ls ' + options.mountpoint)
except socket.timeout:
    print("CRITICAL - Directory listing for " + options.mountpoint + " timed out on host " + getConnectedPeerHostname() + ". | " + PERFDATA)
    sys.exit(STATE_CRITICAL)
except paramiko.ssh_exception.SSHException:
    print("CRITICAL - SSH connection to host " + getConnectedPeerHostname() + " failed. |")
    sys.exit(STATE_CRITICAL)

for mount in fstdout:
    getStats(options.mountpoint + "/" +  mount)

    try:
        fstdout = getCommandMultiOutput('ls ' + options.mountpoint + "/" + mount)
    except socket.timeout:
        print("CRITICAL - Directory listing for " + options.mountpoint + mount + " on host " + getConnectedPeerHostname() + " timed out. | " + PERFDATA)
        sys.exit(STATE_CRITICAL)
    except paramiko.ssh_exception.SSHException:
        print("CRITICAL - SSH connection to host " + getConnectedPeerHostname() + " failed when getting a directory listing for " + options.mountpoint + mount + " |")
        sys.exit(STATE_CRITICAL)

    for folder in fstdout:
        getStats(options.mountpoint + "/" + mount + "/" + folder)

hostname = getCommandSingleOutput("hostname")
currentdatetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

# Write the current time to the test file
try:
    writecommand = getCommandSingleOutput('echo "' + currentdatetime + '" > icingacheck_' + hostname)
except socket.timeout:
    print("CRITICAL - Writing time to testing file icingacheck_" + hostname + " timed out. | " + PERFDATA)
    sys.exit(STATE_CRITICAL)
except paramiko.ssh_exception.SSHException:
    print("CRITICAL - SSH connection to host " + getConnectedPeerHostname() + " failed when writing testing file icingacheck_" + hostname + " |")
    sys.exit(STATE_CRITICAL)

if "permission denied" in writecommand.lower():
    print("CRITICAL - Could not write time to test file on host " + getConnectedPeerHostname() + ". (Permission Denied). | " + PERFDATA)
    sys.exit(STATE_CRITICAL)

# And read it back
try:
    readdatetime = getCommandSingleOutput('cat icingacheck_' + hostname)
except socket.timeout:
    print("CRITICAL - Reading time from testing file icingacheck_" + hostname + " timed out. | " + PERFDATA)

if readdatetime != currentdatetime:
    print("CRITICAL - Time written to test file did not match original value. | " + PERFDATA)
    sys.exit(STATE_CRITICAL)

ssh.close()

print("OK - All tests have passed. | " + PERFDATA)
sys.exit(STATE_OK)

